//
//  Post.swift
//  MobileTest
//
//  Created by Pame  on 2/2/18.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import Foundation
import UIKit

class Post: NSObject {
    
    var userId: Int
    var id: Int
    var title: String
    var body: String
    var isFavourite: Bool
    var isRead: Bool

    init(userId: Int, id: Int, title: String, body: String, isFavourite: Bool = false, isRead: Bool = false) {
        self.userId = userId
        self.id = id
        self.title = title
        self.body = body
        self.isFavourite = isFavourite
        self.isRead = isRead
    }

}
