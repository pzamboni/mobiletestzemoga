//
//  DetailViewController.swift
//  MobileTest
//
//  Created by Pame  on 2/2/18.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var titleDetail: UILabel!
    @IBOutlet weak var userInfoDetail: UILabel!
    @IBOutlet weak var bodyDetail: UITextView!
    @IBOutlet weak var addToFavBt: UIBarButtonItem!
    
   //MARK: Instances
    var post: Post? {
        didSet {
            refreshUI()
        }
    }
    
    func refreshUI() {
        loadViewIfNeeded()
        titleDetail.text = post?.title
        bodyDetail.text = post?.body
        userInfoDetail.text = "User: \(post?.userId.description ?? "")"
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addToFavourites(_ sender: UIBarButtonItem) {
        self.post?.isFavourite = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addToFav"), object: nil, userInfo: ["Post" : self.post as Any])
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DetailViewController: PostSelectionDelegate {
    
    func postSelected(_ newPost: Post) {
        post = newPost
    }
}
