//
//  TableViewTestCell.swift
//  MobileTest
//
//  Created by Pame  on 2/3/18.
//  Copyright © 2018 Zemoga. All rights reserved.
//

import UIKit

class TableViewTestCell: UITableViewCell {
    
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var readView: UIView!
    @IBOutlet weak var favIm: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        readView.layer.cornerRadius = readView.frame.size.width / 2
        readView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
